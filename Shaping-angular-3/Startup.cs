﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Shaping_angular_3.Startup))]
namespace Shaping_angular_3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
